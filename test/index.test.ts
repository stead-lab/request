import { SteadClient } from '../src';

describe('Client', () => {
  it('should require a host to be configured', () => {
    expect.assertions(1);

    try {
      new SteadClient({host: ''});
    } catch (error) {
      expect(error.message).toEqual('A host URL is required.');
    }
  });

  it('should build a versioned base URL', () => {
    const client = new SteadClient({host: 'https://api.example.com', version: 'v1'});
    expect(client.getBaseUrl()).toEqual('https://api.example.com/v1');
  });

  it('should use a custom storage factory', async () => {
    const tokenVal = 'sample';
    const client = new SteadClient({
      host: 'https://example.com',
      storage: {
        async get(key: string): Promise<string> {
          return Promise.resolve(tokenVal);
        },
        async set(key: string, value: string): Promise<any> {
          return null;
        },
        async delete(key: string): Promise<any> {
          return null;
        }
      }
    });

    const token = await client.getToken();
    expect(token).toEqual(tokenVal);
  });
});
