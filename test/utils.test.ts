import { queryBuilder, removeLeadingSlash } from '../src/utils';

describe('Utils', () => {
  it('leading slash is removed', () => {
    const string = '/test';

    expect(removeLeadingSlash(string)).toEqual('test');
  });

  it('should output a filter query string from an object', () => {
    const query = {
      filter: {
        eq: {
          brand: {id: 'sample'}
        }
      }
    };

    const queryString = queryBuilder(query);
    expect(queryString).toEqual('?filter=eq(brand.id,sample)');
  });

  it('should output a filter query string from a string', () => {
    const query = {
      filter: {
        eq: {
          brand: 'sample'
        }
      }
    };

    const queryString = queryBuilder(query);
    expect(queryString).toEqual('?filter=eq(brand,sample)');
  });
});
