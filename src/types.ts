export interface IHeaders {
  [key: string]: string;
}

export type Method = 'POST' | 'GET' | 'DELETE' | 'PUT' | 'PATCH';

interface IObject {
  [key: string]: string | number;
}

export interface IQuery {
  include?: string | string[];
  select?: string | string[];
  limit?: number;
  sort?: string;
  flags?: string[];
  aggregator?: string | any;
  offset?: number;
  filter?: {
    eq?: { [key: string]: IObject | string }
    gt?: { [key: string]: IObject | string }
    lt?: { [key: string]: IObject | string }
    le?: { [key: string]: IObject | string }
    ne?: { [key: string]: IObject | string }
    nin?: { [key: string]: IObject | string }
    like?: { [key: string]: IObject | string }
  };
}

export interface IRequestOpts {
  raw?: boolean;
  plain?: boolean;
  query?: IQuery;
  debug?: boolean;
  isFile?: boolean;
  skipToken?: boolean;
  skipVersion?: boolean;
  headers?: IHeaders;
}

export interface IStorageFactory {
  set(key: string, value: string): Promise<any>;

  get(key: string): Promise<string | null>;

  delete(key: string): Promise<any>;
}

export interface IPreRequestArg {
  endpoint: string,
  data: any,
  method: Method,
  opts: IRequestOpts
}

export interface IPostRequestArg {
  endpoint: string,
  data: any,
  method: Method,
  status: number,
  result: any,
  opts: IRequestOpts
}

export interface IOptions {
  host: string;
  token?: Function | string;
  version?: string;
  headers?: IHeaders;
  storage?: IStorageFactory;
  tokenKey?: string;
  application?: string;
  preRequest?: (request: IPreRequestArg) => Promise<void>;
  postRequest?: (request: IPostRequestArg) => Promise<void>;
}
