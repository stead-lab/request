import { IHeaders, IOptions, IPreRequestArg, IPostRequestArg, IQuery, IRequestOpts, IStorageFactory, Method } from './types';
import { queryBuilder, removeLeadingSlash } from './utils';

export { IQuery, IRequestOpts, IStorageFactory, IOptions };

export class SteadClient {
  readonly host: string;
  private readonly _token?: Function | string;
  private readonly _version?: string;
  private readonly _headers?: IHeaders;
  private readonly _tokenKey: string;
  storage: IStorageFactory;
  preRequest?: (request: IPreRequestArg) => Promise<any>;
  postRequest?: (request: IPostRequestArg) => Promise<any>;

  constructor(options: IOptions) {
    const { host, version, token, tokenKey, storage, preRequest, postRequest, headers, application } = options;

    if (!host) {
      throw new Error('A host URL is required.');
    }

    this.host = host;
    this._version = version;
    this._token = token;
    this.storage = storage;
    this.preRequest = preRequest;
    this.postRequest = postRequest;
    if (!storage && typeof localStorage !== 'undefined') {
      this.storage = {
        set: async (key: string, value: any) => localStorage.setItem(key, value),
        get: async (key: string) => localStorage.getItem(key),
        delete: async (key: string) => localStorage.removeItem(key)
      };
    }
    this._headers = {
      ...(headers || {}),
      ...(application && { 'X-STEAD-CLIENT': application })
    };
    this._tokenKey = tokenKey || '$std_tkn';
  }

  getBaseUrl(skipVersion: boolean = false) {
    return `${this.host}${this._version && !skipVersion ? `/${this._version}` : ''}`;
  }

  async getToken(): Promise<string | boolean> {
    if (typeof this._token === 'function') {
      return this._token();
    } else if (typeof this._token === 'string') {
      return this._token;
    }

    const tk = await this.storage.get(this._tokenKey);
    return tk || false;
  }

  getUrl(endpoint: string, query: IQuery = {}, skipVersion: boolean = false): string {
    return `${this.getBaseUrl(skipVersion)}/${removeLeadingSlash(endpoint)}${queryBuilder(query)}`;
  }

  request(endpoint: string, data: any = null, method: Method = 'GET', opts: IRequestOpts): Promise<any> {
    return new Promise<any>(async (resolve, reject) => {
      if (this.preRequest) {
        await this.preRequest({ endpoint, data, method, opts });
      }

      const token = await this.getToken();

      if (!opts.skipToken && !token) {
        throw new Error('No token available');
      }

      const body = data ? opts.isFile || opts.raw ? data : JSON.stringify({ data }) : undefined;

      const overHeaders: IHeaders = {
        ...(this._headers),
        ...(opts.headers || {})
      };

      const headers = new Headers();

      if (data && !opts.isFile) {
        headers.set('Content-Type', 'application/json');
      }

      if (!opts.skipToken && token) {
        headers.set('Authorization', `Bearer ${token}`);
      }

      Object.keys(overHeaders).forEach(name => headers.set(name, overHeaders[name]));

      return fetch(this.getUrl(endpoint, opts.query, opts.skipVersion), { method, headers, body })
        .then(async r => {
          if (opts.debug) {
            console.debug(method, r.url.replace(this.getBaseUrl(opts.skipVersion), ''), body);
          }

          let result;

          try {
            result = await r.json();
          } catch (e) {
            result = { data: null };
          }

          if (this.postRequest) {
            await this.postRequest({ endpoint, data, method, status: r.status, result, opts });
          }

          if (r.ok) {
            return resolve(opts.plain ? result : result.data);
          }

          reject(result);
        })
        .catch(err => {
          throw err;
        });
    });
  }

  async $get<T = any>(endpoint: string, opts: IRequestOpts = {}): Promise<T> {
    return this.request(endpoint, null, 'GET', opts);
  }

  async $put<T = any, I = any>(endpoint: string, data: I, opts: IRequestOpts = {}): Promise<T> {
    return this.request(endpoint, data, 'PUT', opts);
  }

  async $post<T = any, I = any>(endpoint: string, data?: I, opts: IRequestOpts = {}): Promise<T> {
    return this.request(endpoint, data || null, 'POST', opts);
  }

  async $patch<T = any, I = any>(endpoint: string, data: I, opts: IRequestOpts = {}): Promise<T> {
    return this.request(endpoint, data, 'PATCH', opts);
  }

  async $delete<T = any, I = any>(endpoint: string, data: I = null, opts: IRequestOpts = {}): Promise<T> {
    return this.request(endpoint, data, 'DELETE', opts);
  }
}
