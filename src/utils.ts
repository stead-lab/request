import { IQuery } from './types';

const encode = (str: string) => encodeURIComponent(str);

function formatFilterString(type: string, filter: any) {
  const filterStringArray = Object.keys(filter).map(key => {
    const value = filter[key];
    let queryString: string | string[] = `${key},${encode(value)}`;

    if (typeof value === 'object' && value !== null) {
      if (Array.isArray(value)) {
        queryString = `${key},[${value.join(',')}]`;
      } else {
        queryString = Object.keys(value).map(attr => {
          let objVal = value[attr];
          if (Array.isArray(objVal)) {
            objVal = `[${objVal.join(',')}]`;
          }
          return `${key}.${attr},${encode(objVal)}`;
        });
      }
    }

    return `${type}(${queryString})`;
  });

  return filterStringArray.join(':');
}

function formatQueryString(key: string, value: any) {
  if (key === 'limit' || key === 'offset') {
    return `page${value}`;
  }

  if (key === 'filter') {
    const filterValues = Object.keys(value).map(filter =>
      formatFilterString(filter, value[filter]));
    return `${key}=${filterValues.join(':')}`;
  }

  return `${key}=${value.toString()}`;
}

function buildQueryParams({ include, select, sort, limit, offset, filter, aggregator, flags }: any) {
  const query: any = {
    ...(include ? { include: Array.isArray(include) ? include.map(encode) : encode(include) } : null),
    ...(select ? { select: Array.isArray(select) ? select.map(encode) : encode(select) } : null),
    ...(sort ? { sort: `${sort}` } : null),
    ...(aggregator ? { aggregator: encodeURIComponent(`${JSON.stringify(aggregator)}`) } : null),
    ...(offset ? { offset: `[offset]=${offset}` } : null),
    ...(limit ? { limit: `[limit]=${limit}` } : null),
    ...(filter ? { filter } : null),
    ...(flags ? { flags } : null)
  };

  return Object.keys(query)
    .map((k: string) => formatQueryString(k, query[k]))
    .join('&');
}

export function queryBuilder(query: IQuery): string {
  if (
    query.include ||
    query.sort ||
    query.limit ||
    query.offset ||
    query.select ||
    query.filter ||
    query.flags
  ) {
    return `?${buildQueryParams(query)}`;
  } else {
    return '';
  }
}

export function removeLeadingSlash(string: string) {
  return string.replace(/^\/+/, '');
}
